# Iridium-SR
A KCP packet sniffer + visualizer in one, backend rewritten in Go.

## Credits
- [tamilpp25](https://github.com/tamilpp25/Iridium-SR) (patch sr)<br/>
- [Akka0](https://github.com/Akka0/Iridium-NG) (original)<br/>